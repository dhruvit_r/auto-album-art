//
//  AppDelegate.m
//  AutoAlbumArt
//
//  Created by Dhruvit Raithatha on 11/10/14.
//  Copyright (c) 2014 jakeknight. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate ()

@property (weak) IBOutlet NSWindow *window;

@end

@implementation AppDelegate {
    NSMutableDictionary *thisSongDictionary;
}

@synthesize artworkView;
@synthesize titleLabel;
@synthesize albumLabel;
@synthesize artistLabel;
@synthesize genreLabel;
@synthesize j;
@synthesize selectedTrack;

- (IBAction)nextSongClicked:(id)sender {
    j = 0;
    [iTunes nextTrack];
    while (iTunes.playerPosition == iTunesEPlSPlaying) {
        if (selectedTrack != [iTunes currentTrack]) {
            selectedTrack = [iTunes currentTrack];
            [self grabInfoForCurrentSong];
        }
    }
}

- (IBAction)doneClicked:(id)sender {
    [[selectedTrack.artworks objectAtIndex:0] setData:(NSData*)[[self artworkView] image]];
    NSLog(@"%@ - %@ -> %@ - %@", [self localInfoForCurrentSong][@"trackName"], [self localInfoForCurrentSong][@"artistName"], [[self titleLabel] stringValue], [[self artistLabel] stringValue]);
    [selectedTrack setAlbumArtist:[[self artistLabel] stringValue]];
    [selectedTrack setAlbum:[[self albumLabel] stringValue]];
    [selectedTrack setArtist:[[self artistLabel] stringValue]];
    [selectedTrack setName:[[self titleLabel] stringValue]];
    NSLog(@"%@ - %@ -> %@ - %@", [self localInfoForCurrentSong][@"trackName"], [self localInfoForCurrentSong][@"artistName"], [[self titleLabel] stringValue], [[self artistLabel] stringValue]);
    
    [selectedTrack setGenre:[[self genreLabel] stringValue]];
}

- (IBAction)autoClicked:(id)sender {
    
}

- (IBAction)nextImage:(id)sender {
    j < [thisSongDictionary[@"result"] count] - 1 ? j++ : j;
    [self dictionaryLoaded];
}

- (NSDictionary*)localInfoForCurrentSong {
    
    if (selectedTrack == nil) selectedTrack = [iTunes currentTrack];
    
    NSMutableDictionary *infoDict = [[NSMutableDictionary alloc] init];
    
    NSImage *artwork = (NSImage*)[[selectedTrack.artworks objectAtIndex:0] data];
    NSString *artistName = selectedTrack.artist;
    NSString *albumName = selectedTrack.album;
    NSString *trackName = selectedTrack.name;
    
    if (artwork != nil) infoDict[@"artwork"] = artwork;
    if (artistName != nil) infoDict[@"artistName"] = artistName;
    if (albumName != nil) infoDict[@"albumName"] = albumName;
    if (trackName != nil) infoDict[@"trackName"] = trackName;
    
    return infoDict;
}

- (IBAction)previousImage:(id)sender {
    j  > 0 ? j-- : 0;
    [self dictionaryLoaded];
}

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {
    thisSongDictionary = [[NSMutableDictionary alloc] init];
    j = 0;
    selectedTrack = [iTunes currentTrack];
    iTunes = [SBApplication applicationWithBundleIdentifier:@"com.apple.iTunes"];
    if ([iTunes isRunning]) {
        [self grabInfoForCurrentSong];
    }
}



- (void)grabInfoForCurrentSong {
    
    NSString *query = [NSString stringWithFormat:@"%@ %@", [self localInfoForCurrentSong][@"trackName"], [self localInfoForCurrentSong][@"artistName"]];
    query = [query stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    NSString *urlString = [NSString stringWithFormat: @"https://itunes.apple.com/search?term=%@&media=music", query];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]];
    request.HTTPMethod = @"GET";
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask  *task =
    [session dataTaskWithRequest:request
               completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                   if (error != nil) {
                       return;
                   }
                   NSError *err;
                   NSString *dataString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                   NSDictionary *x = [NSJSONSerialization JSONObjectWithData:[dataString dataUsingEncoding:NSUTF8StringEncoding]
                                                                     options:NSJSONReadingMutableContainers
                                                                       error:&err];
                   
                   thisSongDictionary[@"result"] = x[@"results"];
                   
                   
                   [self dictionaryLoaded];
                   
               }];
    [task resume];
}

- (void)dictionaryLoaded {
    if ([thisSongDictionary[@"result"] count] != 0) {
        
        [[self imageProgressIndicator] setHidden:NO];
        [[self imageProgressIndicator] startAnimation:self];
        
        artworkView.image = nil;
        
        NSDictionary *thisEl = thisSongDictionary[@"result"][j];
        
        if (thisEl[@"artistName"] != nil) artistLabel.stringValue = thisEl[@"artistName"];
        
        if (thisEl[@"primaryGenreName"] != nil) genreLabel.stringValue = thisEl[@"primaryGenreName"];
        
        if (thisEl[@"collectionName"] != nil) albumLabel.stringValue = thisEl[@"collectionName"];
        
        if (thisEl[@"trackName"] != nil) titleLabel.stringValue = thisEl[@"trackName"];
        
        NSString *artworkUrl = thisEl[@"artworkUrl100"];
        artworkUrl = [artworkUrl stringByReplacingOccurrencesOfString:@"100x100" withString:@"1200x1200"];
        
        if (artworkUrl != nil || [artworkUrl isNotEqualTo:@""]) self.artworkView.image = [[NSImage alloc] initWithContentsOfURL:[NSURL URLWithString:artworkUrl]];
        
        [[self imageProgressIndicator] setHidden:YES];
        [[self imageProgressIndicator] stopAnimation:self];
        
        [self doneClicked:nil];
        
        [[[iTunes currentPlaylist] tracks] removeObject:[iTunes currentTrack]];
        
        [self nextSongClicked:nil];
        
    } else {
        
        titleLabel.stringValue = @"Nothing available";
        [self nextSongClicked:nil];
        
    }
    
}


@end
