//
//  AppDelegate.h
//  AutoAlbumArt
//
//  Created by Dhruvit Raithatha on 11/10/14.
//  Copyright (c) 2014 jakeknight. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "iTunes.h"

@interface AppDelegate : NSObject <NSApplicationDelegate> {
    iTunesApplication *iTunes;
}

@property IBOutlet NSImageView *artworkView;
@property IBOutlet NSTextField *titleLabel;
@property IBOutlet NSTextField *albumLabel;
@property IBOutlet NSTextField *artistLabel;
@property IBOutlet NSTextField *genreLabel;
@property IBOutlet NSProgressIndicator *imageProgressIndicator;

@property iTunesTrack *selectedTrack;
@property int j;

- (IBAction)nextSongClicked:(id)sender;
- (IBAction)nextImage:(id)sender;
- (IBAction)previousImage:(id)sender;
- (IBAction)doneClicked:(id)sender;
- (IBAction)autoClicked:(id)sender;

- (void)grabInfoForCurrentSong;
- (NSDictionary*)localInfoForCurrentSong;

@end

