//
//  main.m
//  AutoAlbumArt
//
//  Created by Dhruvit Raithatha on 11/10/14.
//  Copyright (c) 2014 jakeknight. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[]) {
    return NSApplicationMain(argc, argv);
}
